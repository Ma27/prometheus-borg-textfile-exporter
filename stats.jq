def last_archive_timestamp: .archives[-1].start | .[:-7]+"Z" | fromdateiso8601;
def num_archives: .archives | length;
def render_labels: to_entries | map (.key + "=\"" + .value + "\"") | join(",");
def last_archive: .archives | .[0];
def to_metric_lines:
  (if $NEED_HELP == "1" then [
    "# HELP borg_" + .key + " " + .value.help,
    "# TYPE borg_" + .key + " " + .value.type
  ] else [] end)
  + ["borg_" + .key + "{" + (.value.labels | render_labels) + "} " + (.value.value | tostring)];

.
| {
  num_archives: {
    value: .[0] | num_archives,
    type: "gauge",
    help: "Amount of archives",
    labels: { repo: $REPO }
  },
  last_archive_timestamp: {
    value: .[0] | last_archive_timestamp,
    type: "gauge",
    help: "Timestamp of the last backup",
    labels: { repo: $REPO }
  },
  last_backup_duration: {
    value: .[1] | last_archive | .duration,
    type: "gauge",
    help: "Seconds it took to create the last archive",
    labels: { repo: $REPO, archive: .[1] | last_archive | .name }
  },
  last_compressed_size: {
    value: .[1] | last_archive | .stats.compressed_size,
    type: "gauge",
    help: "Compressed size of the last archive in bytes",
    labels: { repo: $REPO, archive: .[1] | last_archive | .name }
  },
  last_deduplicated_size: {
    value: .[1] | last_archive | .stats.deduplicated_size,
    type: "gauge",
    help: "Deduplicated size of the last archive in bytes",
    labels: { repo: $REPO, archive: .[1] | last_archive | .name }
  },
  num_files: {
    value: .[1] | last_archive | .stats.nfiles,
    type: "gauge",
    help: "Number of files in the last backup",
    labels: { repo: $REPO, archive: .[1] | last_archive | .name }
  },
  last_original_size: {
    value: .[1] | last_archive | .stats.original_size,
    type: "gauge",
    help: "Actual size of the last backup",
    labels: { repo: $REPO, archive: .[1] | last_archive | .name }
  },
  total_chunks: {
    value: .[1] | .cache.stats.total_chunks,
    help: "Total number of backup chunks",
    type: "gauge",
    labels: { repo: $REPO }
  },
  total_compressed_size: {
    type: "gauge",
    value: .[1] | .cache.stats.total_csize,
    help: "Total compressed size in bytes",
    labels: { repo: $REPO }
  },
  total_size: {
    type: "gauge",
    value: .[1] | .cache.stats.total_size,
    help: "Total size in bytes of the entire repository",
    labels: { repo: $REPO }
  },
  total_unique_chunks: {
    type: "gauge",
    help: "Number of chunks that are unique (i.e. not deduplicated in any archive)",
    value: .[1] | .cache.stats.total_unique_chunks,
    labels: { repo: $REPO }
  },
  total_unique_size: {
    type: "gauge",
    help: "Total deduplicated size in bytes",
    value: .[1] | .cache.stats.unique_size,
    labels: { repo: $REPO }
  },
  total_unique_size_compressed: {
    type: "gauge",
    help: "Total deduplicated & comprressed size in bytes",
    value: .[1] | .cache.stats.unique_csize,
    labels: { repo: $REPO }
  }
}
| to_entries
| map (to_metric_lines|join("\n") + "\n")
| add
