{
  description = "Gather metrics using the textfile feature of prometheus-node-exporter for borgbackup";
  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixos-22.11;
  outputs = { self, nixpkgs }: {
    overlay = current: prev: {
      prometheus-borg-textfile-exporter = prev.runCommand "prometheus-borg-textfile-exporter-0.0.1"
        { nativeBuildInputs = [ current.makeWrapper ]; }
        ''
          mkdir -p $out/{bin,libexec/prometheus-borg-textfile-exporter}
          cp ${./stats.jq} $out/libexec/prometheus-borg-textfile-exporter/stats.jq
          cp ${./exporter.sh} $out/bin/prometheus-borg-textfile-exporter
          patchShebangs $out/bin
          wrapProgram $out/bin/prometheus-borg-textfile-exporter \
            --prefix PATH : "${current.lib.makeBinPath [ current.jq current.borgbackup ]}" \
            --set STATS_PATH $out/libexec/prometheus-borg-textfile-exporter/stats.jq
        '';
    };
    packages.prometheus-borg-textfile-exporter.x86_64-linux =
      (import nixpkgs { overlays = [ self.overlay ]; system = "x86_64-linux"; }).prometheus-borg-textfile-exporter;
    defaultPackage = self.packages.prometheus-borg-textfile-exporter;
    nixosModules.prometheus-borg-textfile-exporter = { pkgs, lib, config, ... }: with lib;
      let
        cfg = config.services.prometheus-borg-textfile-exporter;
      in {
        options.services.prometheus-borg-textfile-exporter = {
          enable = mkEnableOption "textfile-exporter for borg(1)";
          collectorDir = mkOption {
            type = types.str;
            default = "/run/prometheus-node-exporter-textfiles";
            description = ''
              Target directory where to write the textfile stats to.
            '';
          };
          collectorFile = mkOption {
            type = types.str;
            default = "borg.prom";
            description = ''
              Name of the file inside <xref linkend="opt-services.prometheus-borg-textfile-exporter.collectorDir" />
              where the metrics should eb written to.
            '';
          };
          borgCache = mkOption {
            type = types.str;
            description = ''
              Path to borg's cache-directory.
            '';
          };
          group = mkOption {
            type = types.str;
            description = ''
              Owning group used by <citerefentry><refentrytitle>borg</refentrytitle>
              <manvolnum>1</manvolnum></citerefentry>.
            '';
          };
          repos = mkOption {
            description = ''
              Repos to monitor.
            '';
            example = literalExpression
              ''
                {
                  offsite = {
                    path = "2342@rsync.net:~/backups";
                    keyfile = "/run/secrets/passphrase";
                  };
                }
              '';
            type = types.attrsOf (types.submodule {
              options = {
                path = mkOption {
                  type = types.str;
                  description = ''
                    Path to the remote repository.
                  '';
                };
                keyfile = mkOption {
                  type = types.str;
                  description = ''
                    Path to the keyfile to unlock the repository.
                  '';
                };
                isLocal = mkOption {
                  default = false;
                  type = types.bool;
                  description = ''
                    Whether the borg repo is a local path. Needed to whitelist
                    the path inside systemd hardening.
                  '';
                };
                sshKeyPath = mkOption {
                  default = null;
                  type = types.nullOr types.str;
                  description = ''
                    Path to the identity file used for remote repositories.
                  '';
                };
              };
            });
          };
        };
        config = mkIf cfg.enable {
          assertions = [
            { assertion = config.services.prometheus.exporters.node.enable;
              message = ''
                This exporter only works if `services.prometheus.exporters.node` is enabled!
              '';
            }
          ];
          nixpkgs.overlays = [ self.overlay ];
          systemd.services.prometheus-borg-textfile-exporter = {
            after = [ "prometheus-node-exporter.service" ];
            bindsTo = [ "prometheus-node-exporter.service" ];
            serviceConfig = {
              CacheDirectory = "prometheus-borg-textfile-exporter";
              ExecStart = concatMapStringsSep " " escapeShellArg
                ([
                  "${pkgs.prometheus-borg-textfile-exporter}/bin/prometheus-borg-textfile-exporter"
                  "--collector-dir"  cfg.collectorDir
                  "--collector-file" cfg.collectorFile
                  "--cache-dir"      cfg.borgCache
                ] ++ (flip concatMap (attrNames cfg.repos) (name:
                  [ "--repo" name cfg.repos.${name}.path cfg.repos.${name}.keyfile ]
                  ++ optional (cfg.repos.${name}.sshKeyPath != null) cfg.repos.${name}.sshKeyPath)));
              LockPersonality = true;
              MemoryDenyWriteExecute = true;
              NoNewPrivileges = true;
              PrivateDevices = true;
              PrivateMounts = true;
              PrivateTmp = true;
              ProtectClock = true;
              ProtectHostname = true;
              ProtectKernelLogs = true;
              ProtectKernelModules = true;
              ProtectKernelTunables = true;
              ProtectSystem = "full";
              RemoveIPC = true;
              RestrictAddressFamilies = [ "AF_INET" "AF_INET6" ];
              RestrictNamespaces = true;
              RestrictRealtime = true;
              RestrictSUIDSGID = true;
              SupplementaryGroups = [
                cfg.group
                config.systemd.services.prometheus-node-exporter.serviceConfig.Group
              ];
              Type = "oneshot";
              ReadWritePaths = [ cfg.collectorDir cfg.borgCache ]
                ++ flip concatMap (attrValues cfg.repos)
                  ({ isLocal, path, ... }: optional isLocal path)
                ++ flip concatMap (attrValues cfg.repos)
                  ({ sshKeyPath, ... }: optional (sshKeyPath != null) sshKeyPath);
            };
          };
          systemd.timers.prometheus-borg-textfile-exporter = {
            wantedBy = [ "timers.target" ];
            after = [ "prometheus-node-exporter.service" ];
            bindsTo = [ "prometheus-node-exporter.service" ];
            timerConfig = {
              OnCalendar = "*:0/5";
            };
          };
        };
      };
    hydraJobs = {
      package = self.defaultPackage;
      tests.integration.x86_64-linux =
        with import (nixpkgs + "/nixos/lib/testing-python.nix") { system = "x86_64-linux"; };
        let
          privateKey = nixpkgs.legacyPackages.x86_64-linux.writeText "id_ed25519" ''
            -----BEGIN OPENSSH PRIVATE KEY-----
            b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
            QyNTUxOQAAACBx8UB04Q6Q/fwDFjakHq904PYFzG9pU2TJ9KXpaPMcrwAAAJB+cF5HfnBe
            RwAAAAtzc2gtZWQyNTUxOQAAACBx8UB04Q6Q/fwDFjakHq904PYFzG9pU2TJ9KXpaPMcrw
            AAAEBN75NsJZSpt63faCuaD75Unko0JjlSDxMhYHAPJk2/xXHxQHThDpD9/AMWNqQer3Tg
            9gXMb2lTZMn0pelo8xyvAAAADXJzY2h1ZXR6QGt1cnQ=
            -----END OPENSSH PRIVATE KEY-----
          '';
        in
        makeTest {
          name = "borgbackup-textfile-exporter";
          nodes = {
            backupserver = { pkgs, ... }: {
              services.openssh.enable = true;
              services.borgbackup.repos.repo1 = {
                authorizedKeys = [
                  "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHHxQHThDpD9/AMWNqQer3Tg9gXMb2lTZMn0pelo8xyv root@client"
                ];
                path = "/data/borgbackup";
              };
            };
            client = { pkgs, lib, ... }: let
              passphraseFile = pkgs.writeText "passphrase" "hunter2";
            in {
              imports = [ self.nixosModules.prometheus-borg-textfile-exporter ];
              users.groups.borg = {};
              system.activationScripts.borg-permission-hackery = { # should probably be done via tmpfiles
                text = ''
                  mkdir -p /b0rg/foobar -m0775
                  mkdir /run/prometheus-node-exporter-textfiles -pm0775
                '';
                deps = [ "users" ];
              };
              systemd.services.borgbackup-job-local = {
                environment.BORG_BASE_DIR = "/b0rg/foobar";
                serviceConfig.ReadWritePaths = [ "/b0rg" ];
              };
              systemd.services.borgbackup-job-remote = {
                environment.BORG_BASE_DIR = "/b0rg/foobar";
                serviceConfig.ReadWritePaths = [ "/b0rg" ];
              };
              environment.systemPackages = [ pkgs.borgbackup ];
              services = {
                borgbackup.jobs = {
                  local = {
                    paths = "${pkgs.writeTextDir "fnord" "ohai"}";
                    encryption = {
                      mode = "repokey";
                      passCommand = "cat ${passphraseFile}";
                    };
                    startAt = [];
                    repo = "/b0rg/repo";
                    group = "borg";
                  };
                  remote = {
                    encryption.mode = "repokey";
                    encryption.passCommand = "cat ${passphraseFile}";
                    environment.BORG_RSH = "ssh -o StrictHostKeyChecking=accept-new -i /root/id_ed25519";
                    startAt = [];
                    paths = "${pkgs.writeTextDir "fnord2" "snens"}";
                    repo = "borg@backupserver:.";
                    group = "borg";
                  };
                };
                prometheus-borg-textfile-exporter = {
                  enable = true;
                  group = "borg";
                  borgCache = "/b0rg/foobar";
                  collectorDir = "/run/prometheus-node-exporter-textfiles";
                  repos.local = {
                    path = "/b0rg/repo";
                    keyfile = "${passphraseFile}";
                    isLocal = true;
                  };
                  repos.remote = {
                    path = "borg@backupserver:.";
                    isLocal = true;
                    keyfile = "${passphraseFile}";
                    sshKeyPath = "/root/id_ed25519";
                  };
                };
                prometheus.exporters.node = {
                  enable = true;
                  enabledCollectors = [ "textfile" ];
                  openFirewall = true;
                  extraFlags = [
                    "--collector.textfile.directory=/run/prometheus-node-exporter-textfiles"
                  ];
                };
              };
            };
          };
          testScript = ''
            start_all()
            client.wait_for_unit("multi-user.target")

            with subtest("Remote backup preparation"):
                client.succeed(
                    "cp ${privateKey} /root/id_ed25519"
                )
                client.succeed("chmod 0600 /root/id_ed25519")
                client.succeed("systemctl --wait start borgbackup-job-remote")

            with subtest("No repo available"):
                client.succeed("systemctl --wait start prometheus-borg-textfile-exporter")
                out1 = client.succeed("curl localhost:9100/metrics")
                assert 'borg_avail{repo="/b0rg/repo"} 0' in out1, "Repo cannot be found"
                assert 'borg_lock{repo="/b0rg/repo"} 0' in out1, "Lock is not active"

            with subtest("Local backups and stats received"):
                client.succeed("systemctl start --wait borgbackup-job-local")
                # not going to wait up to 5mins here
                client.succeed("systemctl restart prometheus-borg-textfile-exporter")
                client.wait_until_succeeds("curl localhost:9100/metrics | grep borg_num_archives")
                out2 = client.succeed("curl localhost:9100/metrics|grep borg_")
                assert 'borg_avail{repo="/b0rg/repo"} 1' in out2, "Repo is available"
                assert 'borg_lock{repo="/b0rg/repo"} 0' in out2, "Repo is unlocked"
                assert 'borg_num_archives{repo="/b0rg/repo"} 1' in out2, "Repo has one archive"
                assert [x for x in out2.split('\n') if x.startswith('borg_num_files')][0][-1] == "1", "One file in archive"

            with subtest("Lock handled correctly"):
                client.succeed("${pkgs.writeShellScript "test-lock-behavior" ''
                  export BORG_REPO="/b0rg/repo"
                  export BORG_PASSCOMMAND="echo hunter2"
                  borg with-lock :: ${pkgs.writeShellScript "indef" "while true; do sleep 5; done"} &
                  p=$!
                  borg list 2>&1 | grep 'Failed to create/acquire the lock'
                  systemctl restart prometheus-borg-textfile-exporter
                  t=0
                  while true; do
                    if [ "$t" -gt 60 ]; then
                      echo >&2 "Timeout";
                      kill $p
                      exit 1
                    fi
                    if ! curl localhost:9100/metrics | grep -E '^borg_lock' | grep -E '1$'; then
                      echo >&2 "Waiting..."
                      sleep 1
                      ((t=t+1))
                      continue
                    else
                      out="$(curl localhost:9100/metrics)"
                      set -x
                      grep -E '^borg_lock{repo="/b0rg/repo"} 1$' <<<"$out"
                      grep -E '^borg_num_archives{repo="/b0rg/repo"} 1$' <<<"$out"
                      grep -E '^borg_avail{repo="/b0rg/repo"} 1$' <<<"$out"
                      set +x
                      break
                    fi
                  done
                  kill $p
                  borg list 2>&1 | grep -v 'Failed to create/acquire the lock'
                ''}")

                client.succeed("systemctl restart prometheus-borg-textfile-exporter")
                client.wait_until_succeeds("curl localhost:9100/metrics | grep -E '^borg_lock' | grep -E '0$'")

            with subtest("Scan for remote repo stats"):
                out3 = client.succeed("curl localhost:9100/metrics")
                assert 'borg_avail{repo="borg@backupserver:."} 1' in out3, "Repo is available"
                assert 'borg_lock{repo="borg@backupserver:."} 0' in out3, "Repo is unlocked"
                assert 'borg_num_archives{repo="borg@backupserver:."} 1' in out3, "Repo has one archive"

            client.shutdown()
            backupserver.shutdown()
          '';
        };
    };
  };
}
