#!/usr/bin/env bash

set -euo pipefail

stats_path="${STATS_PATH:-$(pwd)/stats.jq}"
cache_dir="${CACHE_DIRECTORY:-$HOME/.cache/prometheus-borg-textfile-exporter}"
mkdir $cache_dir -p
[ ! -e "$stats_path" ] && exit 1
collector_dir="/run/prometheus-node-exporter"
collector_file="borg.prom"
textfile_owner="node-exporter"
borg_cache_dir="${HOME:-/tmp}/.cache/borg"
declare -A borg_targets
borg_targets=()
declare -A borg_passfiles
declare -A borg_sshkeys

print_error() {
  echo -e >&2 "\e[1;31mError: \e[0m$@"
  exit 1
}

show_help() {
  bold=$'\e[1m'
  reset=$'\e[m'
  tab=$'\t'
  cat >&2 <<EOF
${bold}$(basename "$0"):${reset} retrieve information from borg(1) repositories
and generate Prometheus metrics from it.

${bold}Arguments:${reset}
${tab}${bold}--collector-dir [DIR]${reset}${tab}${tab}${tab}${tab}${tab}Directory where the metrics should be written to.
${tab}${tab}Default: ${bold}/run/prometheus-node-exporter${reset}
${tab}${bold}--collector-file [FILENAME]${reset}${tab}${tab}${tab}${tab}Name of the file where the metrics should be written to.
${tab}${tab}Default: ${bold}borg.prom${reset}
${tab}${bold}--cache-dir [CACHE-DIR]${reset}${tab}${tab}${tab}${tab}${tab}Directory where borg(1) has its caches (to speed up scraping repositories).
${tab}${tab}Default: ${bold}\$HOME/.cache/borg${reset}
${tab}${bold}--textfile-owner [USER]${reset}${tab}${tab}${tab}${tab}${tab}Owning user of the .prom-file.
${tab}${tab}Default: ${bold}node-exporter${reset}
${tab}${bold}--repo [NAME] [BORG PATH] [KEYFILE] [SSH KEYFILE]${reset}${tab}Adds a repository to scrape. Can be supplied multiple times.
${tab}${tab}The ${bold}SSH KEYFILE${reset} argument is optional.
EOF
}

write_stats_avail() {
  target="${1?write_stats_avail requires 5 args!}"
  repo="${2?write_stats_avail requires 5 args!}"
  code="${3?write_stats_avail requires 5 args!}"
  lock_active="${4?write_stats_avail requires 5 args!}"
  need_help="${5?write_stats_avail requires 5 args!}"
  if [ "$need_help" -eq 1 ]; then
    cat >>"$target" <<EOF
# HELP borg_avail Whether the last scrape of a repository was successful
# TYPE borg_avail gauge
borg_avail{repo="$repo"} $code
# HELP borg_lock Whether the lock of the remote repo is currently active
# TYPE borg_lock gauge
borg_lock{repo="$repo"} $lock_active
EOF
  else
    cat >>"$target" <<EOF
borg_avail{repo="$repo"} $code
borg_lock{repo="$repo"} $lock_active
EOF
  fi
}

write_stats_info() {
  target="${1?write_stats_info requires 4 args!}"
  repo="${2?write_stats_info requires 4 args!}"
  json="${3?write_stats_info requires 4 args!}"
  need_help="${4?write_stats_info requires 4 args!}"

  jq <<<"$json" -f "$stats_path" -r --arg "REPO" "$repo" --arg "NEED_HELP" "$need_help" >> "$target"
}

while [ $# -gt 0 ]; do
  case "$1" in
    --help|-h) show_help; exit 0;;
    --cache-dir) borg_cache_dir="${2?--cache-dir requires a value!}";           shift;;
    --collector-dir) collector_dir="${2?--collector-dir requires a value!}";    shift;;
    --collector-file) collector_file="${2?--collector-file requires a value!}"; shift;;
    --textfile-owner) textfile_owner="${2?--textfile-owner requires a value!}"; shift;;
    --repo)
      name="${2?--repo requires a name (arg1)!}";
      path="${3?--repo requires a path (arg2)!}"
      keyfile="${4?--repo requires a path to a keyfile (arg3)!}"
      shift 3
      [ -n "${borg_targets[$name]+exists}" ] && {
        print_error "target $name already exists!"
      }
      borg_targets[$name]="$path"
      borg_passfiles[$name]="$keyfile"
      if [ -e "${2:-}" ]; then
        borg_sshkeys[$name]="$2"; shift;
      fi
      ;;
    *) show_help; exit 1;;
  esac
  shift
done

type borg &>/dev/null || print_error "borg(1) is not in PATH!"
type jq &>/dev/null   || print_error "jq(1) is not in PATH!"

[ ! -w "$collector_dir" ]       && print_error "${collector_dir@Q} is not writable!"
[ ! -w "$borg_cache_dir" ]      && print_error "${borg_cache_dir@Q} is not writable!"
[ "${#borg_targets[@]}" -gt 0 ] || print_error "No repos to monitor were specified!"
tmp_stats_file="$(mktemp)"
trap "rm -f $tmp_stats_file" EXIT QUIT TERM

need_help=1
for name in "${!borg_targets[@]}"; do
  repo="${borg_targets[$name]}"
  keyfile="${borg_passfiles[$name]}"

  # The cache file contains the JSON of the last time $repo was scraped.
  # This is useful if the repo is currently locked (or unavailable) to make
  # sure that no metrics get lost temporarily.
  cache_file="$cache_dir/last_scrape_result-$(base64 -w0 <<<"$repo")"

  # Ensure that any `borg(1)` in the loop can access the repo without additional
  # CLI flags.
  export BORG_BASE_DIR="$borg_cache_dir"
  export BORG_REPO="$repo"
  export BORG_PASSCOMMAND="cat $keyfile"
  if [ -n "${borg_sshkeys[$name]+exists}" ]; then
    export BORG_RSH="ssh -i ${borg_sshkeys[$name]}"
  fi

  # Get a list of all archives of the repo. This check is done for the following reasons:
  # * Gather metrics (amount of archives, timestamp of the last archive)
  # * Check if the repo is available (it is if the lock cannot be acquired)
  # * Check if the repo is locked (because of e.g. a running backup). In that case
  #   the cache is re-used if it exists.
  outfile="$(mktemp)"
  out="$(borg list --json 2>$outfile)" || c=$?
  lock_active=0
  if grep "Failed to create/acquire the lock" "$outfile"; then
    avail=1
    lock_active=1
    if [ -f "$cache_file" ]; then
      json_stats="$(<"$cache_file")"
    fi
  else
    [ "${c:-0}" -ne 0 ] && avail=0 || avail=1
  fi
  rm "$outfile"

  # Write availability stats to the final file. The other stats are merged together
  # with the JSON from `borg info` and then processed by `stats.jq`.
  write_stats_avail "$tmp_stats_file" "$repo" "$avail" "$lock_active" "$need_help"

  # If the repository is available and unlocked, try to run `borg info` to obtain
  # further metrics.
  if [ "${c:-0}" -eq 0 ]; then
    info="$(borg info ::"$(jq -r '.archives|.[-1]|.name' <<<"$out")" --json 2>/dev/null)" || c=$?

    # If the `borg info`-command failed (even though `borg list` succeeded), fall back
    # to the previously cached metrics (if the repo is actually unavailable, `borg list`
    # would've failed already).
    if [ "${c:-0}" -ne 0 ] && [ -z "${json_stats-}" ] && [ -f "$cache_file" ]; then
      json_stats="$(<"$cache_file")"
    fi

    # If `borg info` was actually successful, merge the JSON output from `borg info` and
    # `borg list` into a single string and pass it on to the metrics generator.
    if [ "${c:-0}" -eq 0 ]; then
      json_stats="$(jq -s '[.[0], .[1]]' <(echo "$out") <(echo "$info") -r)"
    fi
  fi

  # Only write metrics and cache them if these could've been obtained from
  # a cache or freshly scraped. This means taht no additional metrics are available if
  # * No cache from a previous scrape was made (i.e. cache is empty).
  # * `borg list` failed OR `borg info` failed.
  if [ -n "${json_stats-}" ]; then
    cat >"$cache_file" <<<"$json_stats"
    write_stats_info "$tmp_stats_file" "$repo" "$json_stats" "$need_help"
  fi

  # HELP/TYPE are only needed once.
  need_help=0
done

install -m0755 -o "$textfile_owner" "$tmp_stats_file" "$collector_dir/$collector_file"
